document.getElementById('loginForm').addEventListener('submit', function(e) {
    e.preventDefault();

    let username = document.getElementById('username').value;
    let password = document.getElementById('password').value;

    // Aquí puedes hacer una comprobación básica de nombre de usuario y contraseña.
    // Por simplicidad, vamos a asumir que el usuario "admin" con contraseña "1234" es válido.
    if (username === "admin" && password === "1234") {
        localStorage.setItem("isLoggedIn", "true"); // Simula que un usuario está logeado
        window.location.href = "store.html";
    } else {
        alert("Invalid credentials!");
    }
});

// Si estás en la tienda, verifica si el usuario está logeado.
if (window.location.pathname.includes("store.html") && localStorage.getItem("isLoggedIn") !== "true") {
    window.location.href = "login.html";
}

let cart = [];

// Función para agregar productos al carrito
function addToCart(product) {
    cart.push(product);
    updateCartUI();
}

function addProductToCart(buttonElement) {
    const productDiv = buttonElement.parentElement;
    const productName = productDiv.querySelector('[data-product-name]').textContent;
    const productPrice = parseFloat(productDiv.querySelector('[data-product-price]').textContent.replace('$', ''));
    addToCart({name: productName, price: productPrice});
}

// Función para quitar productos del carrito
function removeFromCart(product) {
    const index = cart.findIndex(p => p.name === product.name && p.price === product.price);
    if (index !== -1) {
        cart.splice(index, 1);
    }
    updateCartUI();
}

// Actualiza la UI del carrito
function updateCartUI() {
    const cartDiv = document.getElementById('cart');
    cartDiv.innerHTML = ''; // Limpia el contenido actual

    if (cart.length === 0) {
        cartDiv.innerHTML = '<p>Your cart is empty</p>';
    } else {
        cart.forEach(product => {
            const productElement = document.createElement('div');
            productElement.className = 'cart-item';
            productElement.innerHTML = `
                <p>${product.name} - $${product.price}</p>
                <button onclick="removeFromCart({name: '${product.name}', price: ${product.price}})">Remove</button>
            `;
            cartDiv.appendChild(productElement);
        });
    }
}

// Agrega event listeners a los botones "Add to Cart"
document.querySelectorAll('.product button').forEach((button, index) => {
    button.addEventListener('click', () => {
        const product = {
            name: document.querySelectorAll('.product h3')[index].textContent,
            price: parseFloat(document.querySelectorAll('.product p')[index].textContent.replace('$', ''))
        };
        addToCart(product);
    });
});

// Inicializa el carrito vacío en la carga
updateCartUI();