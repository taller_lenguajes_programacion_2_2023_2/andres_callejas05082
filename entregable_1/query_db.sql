DROP TABLE persona CASCADE CONSTRAINTS;

DROP TABLE producto CASCADE CONSTRAINTS;

DROP TABLE venta CASCADE CONSTRAINTS;

-- predefined type, no DDL - MDSYS.SDO_GEOMETRY

-- predefined type, no DDL - XMLTYPE

CREATE TABLE persona (
    id_persona   VARCHAR2(20) NOT NULL,
    nom_completo VARCHAR2(100) NOT NULL,
    correo       VARCHAR2(100) NOT NULL,
    password     VARCHAR2(100) NOT NULL
);

ALTER TABLE persona ADD CONSTRAINT persona_pk PRIMARY KEY ( id_persona );

CREATE TABLE producto (
    id_prod INTEGER NOT NULL,
    nombre  VARCHAR2(100) NOT NULL,
    detalle VARCHAR2(200),
    talla   INTEGER NOT NULL,
    costo   NUMBER NOT NULL,
    precio  NUMBER NOT NULL
);

ALTER TABLE producto ADD CONSTRAINT producto_pk PRIMARY KEY ( id_prod );

CREATE TABLE venta (
    id_venta   INTEGER NOT NULL,
    fecha      DATE NOT NULL,
    id_persona VARCHAR2(20) NOT NULL,
    id_prod    INTEGER NOT NULL,
    cantidad   INTEGER NOT NULL
);

ALTER TABLE venta ADD CONSTRAINT venta_pk PRIMARY KEY ( id_venta,
                                                        fecha );

ALTER TABLE venta
    ADD CONSTRAINT persona_fk FOREIGN KEY ( id_persona )
        REFERENCES persona ( id_persona );

ALTER TABLE venta
    ADD CONSTRAINT producto_fk FOREIGN KEY ( id_prod )
        REFERENCES producto ( id_prod );
