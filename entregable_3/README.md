# Taller de Programación 2

## Fechas de Evaluación

1. **Entregable 1**: 6 septiembre 2023

## Herramientas Utilizadas

- **Git**: [Sitio oficial](https://git-scm.com/)
- **Visual Studio Code**: [Descargar aquí](https://code.visualstudio.com/) Extenciones (Python , Git grafh , Git lens)
- **Python 3.9.2**: [Python descarga](https://www.python.org/ftp/python/3.9.2/python-3.9.2-amd64.exe)

## Control de Versiones

- **git clone** `<url>`
- **git add** .  Empaquetar cambios
- **git commit -m "descripcion del cambio"** Etiquetar version
- **git push origin main** Carga o empuja el paquete a la ubicacion remota

## Espacio de Trabajo

- **python -m venv venv** creacion de espacio de trabajo
- **.\venv\Scripts\activate** activar entorno virtual

## Comandos VSCode

- Control + ñ Terminal

## Sesiones

| Sesión | Fecha      | Tema                                                               |
| ------- | ---------- | ------------------------------------------------------------------ |
| 1       | 09/08/2023 | Presentación y Concertación de evaluación                       |
| 1       | 14/08/2023 | Introducción al control de versiones y su importancia en el curso |
