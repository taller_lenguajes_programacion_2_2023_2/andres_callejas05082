# poo
# objeto
# atributos (caratcteristicas) y funciones (acciones)

# diagrama clases      +   diagrama objetos      +    programacion entidades
#
from conexion import Conexion
import pandas as pd


class Persona(Conexion):
    
    def __init__(
        self, id=0, nom1="", nom2="", apell1="", apell2="", f_nac="", email=""
    ):
        """Esta es la clase personas 
        Args:
            id (int, optional): identificador unico de la . Defaults to 0.
            nom1 (str, optional): _description_. Defaults to "".
            nom2 (str, optional): _description_. Defaults to "".
            apell1 (str, optional): _description_. Defaults to "".
            apell2 (str, optional): _description_. Defaults to "".
            f_nac (str, optional): _description_. Defaults to "".
            email (str, optional): _description_. Defaults to "".
        """
        self.__id = id
        self.__nom1 = nom1
        self.__nom2 = nom2
        self.__apell1 = apell1
        self.__apell2 = apell2
        self.__f_nac = f_nac
        self.__email = email
        super().__init__()
        self.create_Per()
        #E:/taller_programacion2/andres_callejas05082/src/poli_proyecto/static/xlsx/datos_proyecto.xlsx
        ruta = "./src/poli_proyecto/static/xlsx/datos_proyecto.xlsx"
        self.df = pd.read_excel(ruta,sheet_name="personas")
        self.insert_Per()
        #atributos = self.create_Per()
        #print(atributos)

        @property
        def _id(self):
            return self.__id

        @_id.setter
        def _id(self, value):
            self.__id = value

        # @property
        # def _nom1(self):
        #     return self.__nom1

        # @_nom1.setter
        # def _nom1(self, value):
        #     self.__nom1 = value

        # @property
        # def _nom2(self):
        #     return self.__nom2

        # @_nom2.setter
        # def _nom2(self, value):
        #     self.__nom2 = value

        # @property
        # def _apell1(self):
        #     return self.__apell1

        # @_apell1.setter
        # def _apell1(self, value):
        #     self.__apell1 = value

        # @property
        # def _apell2(self):
        #     return self.__apell2

        # @_apell2.setter
        # def _apell2(self, value):
        #     self.__apell2 = value

        # @property
        # def _f_nac(self):
        #     return self.__f_nac

        # @_f_nac.setter
        # def _f_nac(self, value):
        #     self.__f_nac = value

        # @property
        # def _email(self):
        #     return self.__email

        # @_email.setter
        # def _email(self, value):
        #     self.__email = value

        
    def create_Per(self):
        atributos = vars(self)
        if self.crear_tabla(nom_tabla="personas",datos_tbl="datos_per"):
            print("Tabla Personas Creada!!!")
        
        return atributos
    
    def insert_Per(self):
        datos=""
        for index, row in self.df.iterrows():
            datos = '{},"{}","{}","{}","{}","{}",{},{},"{}","{}"'.format(row["id"],row["nom1"],row["nom2"],row["apell1"],row["apell2"],row["f_nac"],row["tel"],row["cel"],row["email"],row["descrip"])
            self.insertar_datos(nom_tabla="personas",nom_columns="carga_per",datos_carga=datos)
        return True
    
    def update_Per(self,id=0):
        return True
    
    def delete_Per(self,id=0):
        return True
    
    def select_Per(self,id=[]):
        return Persona
        
    
        
    def __str__(self) :
        """funcion para visualizar objeto persona
        Args:
            no tiene parametros 
        """
        return "Persona : id {}  nombre {}".format(self.__id,self.__nom1)

