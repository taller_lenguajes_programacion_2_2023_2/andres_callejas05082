let cart = []

function addToCart(product){
  cart.push(product);
  updateCart();
}

function updateCart(){
  let carItems = document.querySelector('.cart-items');
  carItems.innerHTML='';
  let totalPrice = 0;

  cart.forEach(product => {
    let li = document.createElement('li');
    li.textContent = product.name + ' - $'+ product.price;
    carItems.appendChild(li);
    totalPrice += product.price;
  });
  document.querySelector('.total-price').textContent = 'Total: $' + totalPrice
}