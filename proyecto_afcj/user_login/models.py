from django.db import models



# Create your models here.


class Persona(models.Model):
    id = models.IntegerField(primary_key=True)
    nom1 = models.CharField(max_length=100)
    nom2 = models.CharField(max_length=100)
    apell1 = models.CharField(max_length=100)
    apell2 = models.CharField(max_length=100)
    f_nac = models.DateField()
    email = models.CharField(max_length=100)